// RID: b505192x3.prod
// Test at: http://ktest.heroku.com/b505192x3
// Rotten Tomatoes API Key: pk883h59zvtvheytpvqcqrdq
// 
// Create a rotten_tomatoes ruleset and register it using KDK.
// Define a function in the global block of the ruleset that uses http:get() to interact with the movies.json endpoint of the Rotten Tomatoes API. Be sure to include your API key as the first argument of the query string. Note that if you can't retrieve the data using curl or a browser, your function won't be able to either. 
// Use the techniques from Web Rule Exercises and the Quickstart, build a SquareTag app that displays a form that asks the user for a movie title. 
// The rule that responds to the web:submit event should extract the movie title from the the form data and query the Rotten Tomatoes datasource with the movie title and retrieve the desired information (e.g., critic and audience ratings) from the JSON response using the pick() operator.
// The rule should then display the following information on SquareTag: 
// 	Movie thumbnail
// 	Title
// 	Release Year
// 	Synopsis
// 	Critic ratings
// 	and other data you find interesting. 
// The data should be nicely formatted and allow the user to type another movie title in at the bottom. 
// If the movie isn't found, display a nice error message that returns the title the user put in. 

ruleset rotten_tomatoes {
	meta {
		name "Lab 4 - Rotten Tomatoes API"
		description <<
			My Lab 4 ruleset for CS 462.
		>>
		author "Ben Draut"
		logging off
		use module a169x701 alias CloudRain
		use module a41x196  alias SquareTag
	}
	global {
		getMovie = function(searchTerm) {
			http:get('http://api.rottentomatoes.com/api/public/v1.0/movies.json', 
				{'apikey': 'pk883h59zvtvheytpvqcqrdq', 'q': searchTerm, 'page_limit': 1, 'page': 1});
		}
	}
	rule init_dom {
		select when web cloudAppSelected
		pre {
			dom = << 
				<div id="movie-data">
					
				</div>
				<div id="movie-form">
					<h3>Get Movie Info!</h3>
					<form id="lab4-form">
						<p>Movie Title: <input type="text" name="title"/></p>
						<p><input type="submit" value="Submit"/></p>
					</form>
				</div>
			>>
		}
		{
			SquareTag:inject_styling();
      		CloudRain:createLoadPanel("Movie Info", {}, dom);
			watch('#lab4-form', 'submit');
		}
	}
	rule obtain_rating {
		select when web submit '#lab4-form'
		pre {
			movieTitle = event:attr('title');
			response = getMovie(movieTitle);
			data = response.pick("$..content").decode();

			total = data.pick("$.total");

			thumbUrl = data.pick("$.movies[0].posters.thumbnail");
			title = data.pick("$.movies[0].title");
			releaseYear = data.pick("$.movies[0].year");
			synopsis = data.pick("$.movies[0].synopsis");
			criticScore = data.pick("$.movies[0].ratings.critics_score");
			info = <<
				<div>
					<h3>Movie Data:</h3>
					<p>
						<h4>#{title} (#{releaseYear}) - Critic Rating: #{criticScore}</h4>
						<img src="#{thumbUrl}"/>
						<br/>
						<span>#{synopsis}</span>
					</p>
				</div>
			>>;

			err = <<
				<p>Sorry, we couldn't find any results for "#{movieTitle}". Please try something else.</p>
			>>;

			insert = total > 0 => info | err;
		}
		{
			replace_inner('#movie-data', insert);
		}
	}
}
