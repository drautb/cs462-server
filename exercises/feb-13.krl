// 
// Assume the following event streams:
// 
// Twitter stream (tweet:received) with attributes body and from
// Email IMAP account (email:received, email:sent, email:forwarded) with attributes, to, from, subj, and body
// Stock price update stream (stock:update) with attribute ticker, price, change, percent, and name)
// Web pageview (web:pageview) with attributes url, title, and referer.
// Write event expressions that select the following:
// 
// 
// Make up two scenarios involving a complex event expression and the preceding event streams. Write event expressions for them.


// Tweets containing the keyword "healthcare"
select when tweet:received body re#healthcare#

// Emails with a body containing the words "BYU" and "football" in any order
select when email:received body re#BYU|football#
		 or email:sent body re#BYU|football#
		 or email:forwarded body re#BYU|football#

// Four tweets with the keyword "healthcare" within 4 hours
select when count 4 tweet:received body re#healthcare#
		within 4 hours

// Tweet with keyword "healthcare" followed by an email with "healthcare" in the body or subject
select when tweet:received body re#healthcare# 
		before any 1 (email:received body re#healthcare#,
					  email:sent body re#healthcare#,
					  email:forwarded body re#healthcare#)

// More than five emails from the same person within a 20 minute period
select when count 6 email:received from 

// Tweets that contain a stock-ticker symbol and the price of that same stock goes up by more than 2 percent within 10 minutes:


// User visits any two of Google, Yahoo!, MSNBC, CNN, or KSL.
