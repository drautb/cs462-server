// RID: b505192x2
// Write a rule called show_form that inserts text paragraph (make up the text) 
// in the <div/> element with the ID of main on ktest.heroku.com. 
// 
// Modify the show_form rule from (1) to place a simple Web form that has fields for a first name, 
// a last name, and a submit button. Use the watch() action to watch it for activity. 
// 
// Write a rule that selects on submit and takes the first and last name 
// from the form in (3) and stores them in entity variables.
// 
// Modify the ruleset so that if a first and last name have been stored, they are displayed 
// in the page (in a paragraph under the form) and if they are not, the form is displayed. 
// 
// Add a clear rule that clears the names if the query string clear=1 is added to the URL.

ruleset Lab3 {
	meta {
		name "Lab 3 - Forms"
		description <<
			My Lab 3 ruleset for CS 462.
		>>
		author "Ben Draut"
		logging off
	}
	rule clear_data {
		select when pageview re#\?.*clear=1#
		notify("Clearing Data...", "Name data has been cleared.");
		always {
			clear ent:fname;
			clear ent:lname;
		}
	}
	rule show_form {
		select when pageview ".*" setting ()
		pre {
			form = <<
				<div>
					<h3>Lab 3 Form</h3>
					<form id="lab3-form">
						<p>First Name: <input type="text" name="fname"/></p>
						<p>Last Name: <input type="text" name="lname"/></p>
						<p><input type="submit" value="Submit"/></p>
					</form>
				</div>
			>>
		}
		{
			replace_inner('#main', form);
			watch('#lab3-form', 'submit');			
		}
	}
	rule show_greeting {
		select when pageview ".*" setting()
		pre {
			name = ent:fname + " " + ent:lname;
			greeting = <<
				<div>
					<h3>Hello, #{name}</h3>
				</div>
			>>
		}
		if ent:fname then {
			replace_inner('#main', greeting);
		}
	}
	rule submit_clicked {
		select when web submit "#lab3-form"
		pre {
			fname = event:attr("fname");
			lname = event:attr("lname");
			greeting = <<
				<div>
					<h3>Hello, #{fname} #{lname}</h3>
				</div>
			>>
		}
		replace_inner("#main", greeting);
		always {
			set ent:fname fname;
			set ent:lname lname;
		}
	}
}


