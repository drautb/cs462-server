// Write a location_catch rule that listens for location:notification events
// Store the latest location data in an entity variable
// Write a location_show rule that shows the location in SquareTag (see Quickstart). 
//
// b505771x0.prod - squaretag1
// b505772x0.prod - squaretag2
//
// b505926x0.prod - squaretag3
// b505927x0.prod - squaretag4
ruleset lab8_location {
	meta {
		name "Lab 8"
		description <<
			My Lab 8 ruleset for CS 462
		>>
		author "Ben Draut"
		logging off
		use module a169x701 alias CloudRain
		use module a41x196  alias SquareTag
	}
	rule location_catch {
		select when location notification
		pre {
			checkin_data = event:attr('checkin').decode();
			venue_name = checkin_data.pick("$..venue.name");
			city = checkin_data.pick("$..venue.location.city");
			shout = checkin_data.pick("$..shout");
			created_at = checkin_data.pick("$..createdAt");
			latitude = checkin_data.pick("$..lat");
			longitude = checkin_data.pick("$..lng");
			basic_data = {"venue": venue_name, "city": city, "shout": shout, "createdAt": created_at, "latitude": latitude, "longitude": longitude};
		}
		{
			noop();
		}
		always {
			set ent:basic_data basic_data;
			set ent:checkin_data checkin_data;
		}
	}
	rule location_show {
		select when web cloudAppSelected
		pre {
			checkin_data = ent:basic_data;
			venue = checkin_data{"venue"};
			city = checkin_data{"city"};
			shout = checkin_data{"shout"};
			createdAt = checkin_data{"createdAt"};
			lat = checkin_data{"latitude"};
			long = checkin_data{"longitude"};
			myData = <<
				<h4>Lab 8 - Most Recent Checkin Data:</h4>
				<p>
					<b>Venue Name:</b> #{venue}</br>
					<b>City: </b> #{city}<br/>
					<b>Shout: </b> #{shout}<br/>
					<b>Created At: </b> #{createdAt}<br/>
					<b>Location: </b> #{lat}, #{long}<br/>
				</p>
			>>
		}
		{
			SquareTag:inject_styling();
			CloudRain:createLoadPanel("Checkin Info", {}, myData);
		}
		always {
			log ent:checkin_data;
		}
	}
}