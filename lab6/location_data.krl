// RID: b505192x5.prod

ruleset location_data {
	meta {
		name "Lab 6 - Location Data"
		description <<
			My Lab 6 ruleset for CS 462
		>>
		author "Ben Draut"
		logging off
		provides get_location_data		
	}
	global {
		get_location_data = function(k) {
			ent:kvMap >< k => ent:kvMap{k}
						    | "Key does not exist: #{k}";
		}
	}
	rule add_location_item {
		select when explicit new_location_data
		pre {
			k = event:attr("key");
			v = event:attr("value");
		}
		send_directive(k) with value = {"location": v};
		always {
			set ent:kvMap{k} v;
		}
	}
}
