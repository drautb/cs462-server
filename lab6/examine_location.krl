// RID: b505192x6.prod
// Test at: http://ktest.heroku.com/b505192x6

ruleset examine_location {
	meta {
		name "Lab 6 - Examine Location"
		description <<
			My Lab 6 ruleset for CS 462
		>>
		author "Ben Draut"
		logging off
		key twilio {
		    "account_sid" : "ACb7e6dadc1e0af0443572b02f43e568a5",
		    "auth_token"  : "ef0b58e2f5a6a9841059a9c42bc73c11"
		}
		use module a8x115 alias twilio with twiliokeys = keys:twilio()
		use module b505192x5 alias location_data
		use module a169x701 alias CloudRain
		use module a41x196  alias SquareTag
	}
	rule show_fs_location {
		select when web cloudAppSelected
		pre {
			checkin_data = location_data:get_location_data("fs_checkin");
			venue_name = checkin_data.pick("$..venue");
			city = checkin_data.pick("$..city");
			shout = checkin_data.pick("$..shout");
			created_at = checkin_data.pick("$..createdAt");
			latitude = checkin_data.pick("$..latitude");
			longitude = checkin_data.pick("$..longitude");
			data = <<
				<h4>Most Recent Checkin Data:</h4>
				<p>
					<b>Venue Name:</b> #{venue_name}</br>
					<b>City: </b> #{city}<br/>
					<b>Shout: </b> #{shout}<br/>
					<b>Created At: </b> #{created_at}<br/>
					<b>Location: </b> #{latitude}, #{longitude}<br/>
				</p>
			>>
		}
		{
			twilio:send_sms("+18018200316", "+13852091028", "Hey Ben, you just saw a new checkin!");
			SquareTag:inject_styling();
			CloudRain:createLoadPanel("Checkin Info", {}, data);
		}
	}
}
