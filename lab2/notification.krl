// RID: b505192x1
// See it in action at http://ktest.heroku.com/b505192x1
// Do this to clear the notification counter: http://ktest.heroku.com/b505192x1?clear=true
// If I wanted the notification count to be global, rather than just for the current user,
// I would just use the app: prefix rather than ent: on the count variable. 
ruleset Lab3Notification {
	meta {
		name "Lab 3 - Notification"
		description <<
			My Lab 3 Notification ruleset for CS 462.
		>>
		author "Ben Draut"
		logging off
	}
	global {
   
	}
	rule basic_notifications {
		select when pageview ".*" setting ()
		if 1 == 1 then {
			notify("Hello!", "Welcome to Lab 3.") with sticky = true;
			notify("Also...", "This is a second notification, it should fade momentarily...") with life = 5000;
		}
	}
	rule query_notification {
		select when pageview ".*" setting ()
		pre {
			getName = function(string) {
				name = string.extract(re/name=([^&]*)/);
				name.head() neq "" => name.head() | "Monkey";
			};
			queryStr = getName(page:url("query"));
		}
		notify("Query Notification", "Hello " + queryStr) with sticky = true;
	}
	rule notification_counter {
		select when pageview ".*" setting ()
		if ent:count < 5 then {
			notify("Notification Counter", "Count: " + ent:count);
		} fired {
			ent:count += 1 from 1;
		}
	}
	rule clear_counter {
		select when pageview ".*" setting()
		if page:url("query").match(re/clear=/) then {
			notify("Notification Count Cleared", "");
		} fired {
			set ent:count 0;
		}
	}
}