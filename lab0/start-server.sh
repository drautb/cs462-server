#!/bin/bash

# Enable port forwarding by uncommenting a line in /etc/sysctl.conf
line="net.ipv4.ip_forward"
sed -i "/${line}/ s/# *//" /etc/sysctl.conf
sysctl -p /etc/sysctl.conf

# Forward port 80 to 8000 and open the firewall
iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8000
iptables -A INPUT -p tcp -m tcp --sport 80 -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT

# Update code from GitHub
cd ~
git clone https://github.com/drautb/cs462-server.git
cd ~/cs462-server/lab0

# Start Server
node lab0.js

