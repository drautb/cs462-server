// RID: b505192x4.prod
// Test at: http://ktest.heroku.com/b505192x4
// 
// Create an event channel in your Kynetx account labeled 
// "Foursquare Checkins" using KDK (Kynetx Developer Kit)
// 
// Channel Info:
// Name: foursquare-channel
// ID: 0C6253AC-9FC1-11E3-BE54-C1E6E71C24E1
//
// Use the channel ID to formulate an event signal URL (ESL) 
// that uses the alternate form to raise a foursquare:checkin event
// 
//	http://cs.kobj.net/sky/event/0C6253AC-9FC1-11E3-BE54-C1E6E71C24E1/3934/foursquare/checkin?_rids=b505192x4
//
// Create a foursquare ruleset and activate it in your PEN:
// 	 Write a rule called process_fs_checkin that listens for the foursquare:checkin event. 
//
//   The rule should store the venue name, city, shout, and createdAt event attributes in 
//   entity variables. 
//
//   Write a rule called display_checkin that shows the results in SquareTag.
//
//   You will need to register the ruleset and install it in your SquareTag account. 
// 
// Test the ruleset by using curl or your browser to call the ESL you created above.
// You will have to give it event attributes.
// 
// Create an application at Foursquare, manually go through the OAuth sequence for it, 
// and configure the Push URL to contain the ESL you created above. Remember that 
// Foursquare require https. 
// 
// Use the Foursquare test button to raise the event from Foursquare. 
// You should see results on SquareTag. 
// 
// Checkin with Foursquare on your phone. You should see the result in 
// your SquareTag application. 
//
//
// It says that the domain and the type are the only two requirements. However, 
// for the lab to work, you also need to include the key "rids" in your url.
// ie:
// /sky/event/<eci>/<eid>/<domain>/<type>?_rids=b502807x10
//
//
// https://cs.kobj.net/sky/event/0C6253AC-9FC1-11E3-BE54-C1E6E71C24E1/3934/foursquare/checkin?_rids=b505192x4&checkin={%22id%22:%20%2252f018b9498e9bd452bc05a9%22,%22createdAt%22:%201391466681,%22type%22:%20%22checkin%22,%22shout%22:%20%22Working%20hard%20on%20a%20new%20intramural%20program...%22,%22timeZone%22:%20%22America/Denver%22,%22timeZoneOffset%22:%20-420,%22user%22:%20{%22id%22:%20%2276380168%22,%22firstName%22:%20%22Ben%22,%22lastName%22:%20%22Draut%22,%22gender%22:%20%22male%22,%22relationship%22:%20%22self%22,%22photo%22:%20%22https://is0.4sqi.net/userpix_thumbs/76380168-TFVQIY3TFGC3Q21A.jpg%22,%22tips%22:%20{%22count%22:%200},%22lists%22:%20{%22groups%22:%20[{%22type%22:%20%22created%22,%22count%22:%201,%22items%22:%20[]}]},%22homeCity%22:%20%22Provo,%20Utah%22,%22bio%22:%20%22%22,%22contact%22:%20{%22email%22:%20%22drautb@gmail.com%22,%22facebook%22:%20%22675277677%22}},%22venue%22:%20{%22id%22:%20%224bb0f8eef964a520206f3ce3%22,%22name%22:%20%22Richards%20Building%22,%22contact%22:%20{},%22location%22:%20{%22address%22:%20%22Brigham%20Young%20University%22,%22lat%22:%2040.248972264053,%22lng%22:%20-111.6534948348999,%22postalCode%22:%20%2284602%22,%22cc%22:%20%22US%22,%22city%22:%20%22Provo%22,%22state%22:%20%22UT%22,%22country%22:%20%22United%20States%22},%22categories%22:%20[{%22id%22:%20%224bf58dd8d48988d1b2941735%22,%22name%22:%20%22College%20Gym%22,%22pluralName%22:%20%22College%20Gyms%22,%22shortName%22:%20%22Gym%22,%22icon%22:%20%22https://ss1.4sqi.net/img/categories/education/gym.png%22,%22parents%22:%20[%22College%20&%20University%22],%22primary%22:%20true}],%22verified%22:%20false,%22stats%22:%20{%22checkinsCount%22:%20994,%22usersCount%22:%20230,%22tipCount%22:%203},%22likes%22:%20{%22count%22:%200,%22groups%22:%20[]},%22beenHere%22:%20{%22count%22:%200}}}
// 
// LAB 7 ECIS
// drautb+squaretag1@gmail.com: DD152DD0-B394-11E3-AAEB-0CFC637EDFE5
// drautb+squaretag2@gmail.com: FA98E2B6-B394-11E3-8BB9-E0ABD61CF0AC
//
// drautb+squaretag3@gmail.com: FE4033D4-B55A-11E3-BC83-B8B287B7806A
// drautb+squaretag4@gmail.com: 
ruleset foursquare_checkin {
	meta {
		name "Lab 5 - Foursquare Checkin"
		description <<
			My Lab 5 ruleset for CS 462
		>>
		author "Ben Draut"
		logging off
		use module a169x701 alias CloudRain
		use module a41x196  alias SquareTag
	}
	global {
		sub_map = 
		[
			{ "cid": "DD152DD0-B394-11E3-AAEB-0CFC637EDFE5" },
			{ "cid": "FA98E2B6-B394-11E3-8BB9-E0ABD61CF0AC" }
		]
	}
	rule process_fs_checkin {
		select when foursquare checkin
		pre {
			checkin = event:attr('checkin');
			checkin_data = checkin.decode();

			venue_name = checkin_data.pick("$..venue.name");
			city = checkin_data.pick("$..venue.location.city");
			shout = checkin_data.pick("$..shout");
			created_at = checkin_data.pick("$..createdAt");
			latitude = checkin_data.pick("$..lat");
			longitude = checkin_data.pick("$..lng");
			basic_data = {"venue": venue_name, "city": city, "shout": shout, "createdAt": created_at, "latitude": latitude, "longitude": longitude};
		}
		{
			noop();
		}
		always {
			set ent:venue_name venue_name;
			set ent:city city;
			set ent:shout shout;
			set ent:created_at created_at;
			set ent:latitude latitude;
			set ent:longitude longitude;

			raise explicit event new_location_data 
				for b505192x5 
				with key = "fs_checkin" and value = basic_data;

			raise explicit event dispatch_data with checkin = checkin;
		}
	}
	rule dispatcher {
		select when explicit dispatch_data
			foreach sub_map setting (sub)
			{	
				event:send(sub, "location", "notification") with attrs = { "checkin": event:attr("checkin") };
			}
	}
	rule display_checkin {
		select when web cloudAppSelected
		pre {
			venue_name = ent:venue_name;
			city = ent:city;
			shout = ent:shout;
			created_at = ent:created_at;
			latitude = ent:latitude;
			longitude = ent:longitude;
			data = <<
				<h4>Most Recent Checkin Data:</h4>
				<p>
					<b>Venue Name:</b> #{venue_name}</br>
					<b>City: </b> #{city}<br/>
					<b>Shout: </b> #{shout}<br/>
					<b>Created At: </b> #{created_at}<br/>
					<b>Location: </b> #{latitude}, #{longitude}<br/>
				</p>
			>>
		}
		{
			SquareTag:inject_styling();
			CloudRain:createLoadPanel("Checkin Info", {}, data);
		}
	}
}