var r = require('rethinkdb'),
	config = require('../config/config.json');

module.exports.setup = function () {
	this.onConnect(function (err, conn) {
		// Create the db if it doesn't exist
		r.dbCreate(config.db.name).run(conn, function (err, res) {
			if (err)
				console.log(err.msg);
			else
				console.log("Created Foursquare Database...");

			r.db(config.db.name).tableCreate(config.db.table).run(conn, function (err, res) {
				if (err)
					console.log(err.msg);
				else
					console.log("Created User Table...");
			});

			r.db(config.db.name).tableCreate("config").run(conn, function (err, res) {
				if (err)
					console.log(err.msg);
				else {
					console.log("Created Config Table...");
					r.db(config.db.name).table('config').insert({
						cid: 1,
						client_id: "UNINITIALIZED",
						client_secret: "UNINITIALIZED",
						redirect_uri: "UNINITIALIZED"
					}).run(conn, function (err, res) {
						if (err) console.log(err.msg);
						else console.log("Inserted config row...");
					});
				}
			});
		});
	});
}

module.exports.onConnect = function (callback) {
	r.connect({
		host: config.db.host,
		port: config.db.port
	}, function (err, conn) {
		if (err) throw err;
		callback(err, conn);
	});
}