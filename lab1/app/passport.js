var LocalStrategy = require('passport-local')
	.Strategy,
	db = require('./db'),
	User = require('./models/user'),
	bcrypt = require('bcrypt');

module.exports = function (passport) {

	passport.serializeUser(function (user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function (id, done) {
		User.findById(id, function (err, user) {
			done(err, user);
		});
	});

	passport.use('local-signup', new LocalStrategy(
		function (name, password, done) {
			User.findOne({
				name: name
			}, function (err, user) {
				if (err) return done(err);

				if (user) {
					return done(null, false);
				} else {
					bcrypt.genSalt(10, function (err, salt) {
						bcrypt.hash(password, salt, function (err, hash) {
							var user = {
								name: name,
								salt: salt,
								hash: hash,
								access_token: ''
							};

							User.save(user, function (err, savedUser) {
								if (err) throw err;
								return done(null, savedUser);
							});
						});
					});
				}

			});

		}));

	passport.use('local-login', new LocalStrategy(
		function (name, password, done) {
			User.findOne({
				name: name
			}, function (err, user) {
				// if there are any errors, return the error before anything else
				if (err) return done(err);

				// if no user is found, return the message
				if (!user)
					return done(null, false);

				// if the user is found but the password is wrong
				if (!User.validPassword(user, password))
					return done(null, false);

				// all is well, return successful user
				return done(null, user);
			});

		}));
};