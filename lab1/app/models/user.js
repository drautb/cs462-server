var db = require('../db'),
	r = require('rethinkdb'),
	bcrypt = require('bcrypt'),
	config = require('../../config/config.json');

var User = {
	findAll: function (callback) {
		db.onConnect(function (err, conn) {
			r.db(config.db.name).table(config.db.table).filter(true).run(conn, function (err, cursor) {
				var users = [];
				while (cursor.hasNext()) {
					cursor.next(function (err, row) {
						if (err) throw err;
						users.push(row);
						if (!cursor.hasNext())
							callback(users);
					});
				}
			});
		});
	},

	findById: function (id, callback) {
		db.onConnect(function (err, conn) {
			r.db(config.db.name).table(config.db.table).get(id).run(conn, function (err, res) {
				if (err) throw err;
				if (res === null)
					callback("User doesn't exist", null);
				else
					callback(null, res);
			});
		});
	},

	findOne: function (criteria, callback) {
		db.onConnect(function (err, conn) {
			r.db(config.db.name).table(config.db.table).filter(criteria).limit(1).run(conn, function (err, cursor) {
				if (err) throw err;

				if (cursor.hasNext()) {
					cursor.next(function (err, row) {
						callback(err, row);
					});
				} else {
					callback(null, false);
				}
			});
		});
	},

	save: function (user, callback) {
		db.onConnect(function (err, conn) {
			r.db(config.db.name).table(config.db.table)
				.insert(user, {
					returnVals: true,
					upsert: true
				})
				.run(conn, function (err, res) {
					callback(err, res.new_val);
				});
		});
	},

	validPassword: function (user, password) {
		return bcrypt.compareSync(password, user.hash);
	}
}

module.exports = User;