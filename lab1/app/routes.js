var db = require('../app/db'),
	r = require('rethinkdb'),
	config = require('../config/config.json'),
	request = require('request'),
	u = require('../app/models/user');

module.exports = function (app, passport) {

	// Backend routes go here...
	app.post('/login', passport.authenticate('local-login'), function (req, res) {
		res.send(req.user);
	});

	app.get('/loggedin', function (req, res) {
		res.send(req.isAuthenticated() ? req.user : '0');
	});

	app.post('/signup', passport.authenticate('local-signup'), function (req, res) {
		res.send(req.user);
	});

	app.get('/users', function (req, res) {
		u.findAll(function (users) {
			res.json(users);
		});
	})

	app.get('/profile-data', auth, function (req, res) {
		res.json(req.user);
	});

	app.get('/user-data/:name', function (req, res) {
		u.findOne({
			name: req.params.name
		}, function (err, user) {
			if (err) res.json({
				name: "UNKNOWN",
				checkin: {
					name: "UNKNOWN"
				}
			});

			if (user.access_token !== "") {
				request('https://api.foursquare.com/v2/users/self/checkins?oauth_token=' + user.access_token + "&v=20140131", function (err, result, body) {
					body = JSON.parse(body);
					res.json({
						name: user.name,
						checkin: {
							name: body.response.checkins.items[0].venue.name
						}
					});
				});
			} else res.json({
				name: user.name,
				checkin: {
					name: "UNKNOWN"
				}
			});
		});
	});

	app.post('/logout', function (req, res) {
		req.logOut();
		res.send(200);
	});

	app.get('/foursquare-auth-url', function (req, res) {
		db.onConnect(function (err, conn) {
			r.db(config.db.name).table('config').filter({
				cid: 1
			}).run(conn, function (err, cursor) {
				if (cursor.hasNext()) {
					cursor.next(function (err, row) {
						if (err) throw err;

						var url = 'https://foursquare.com/oauth2/authenticate?client_id=' + encodeURIComponent(row.client_id) +
							'&response_type=code&redirect_uri=' + encodeURIComponent(row.redirect_uri);

						res.send({
							url: url
						});
					});
				}
			});
		});
	});

	app.get('/code', auth, function (req, response) {
		var code = req.query.code;

		db.onConnect(function (err, conn) {
			r.db(config.db.name).table('config').filter({
				cid: 1
			}).run(conn, function (err, cursor) {
				if (cursor.hasNext()) {
					cursor.next(function (err, row) {
						if (err) throw err;

						var url = 'https://foursquare.com/oauth2/access_token?client_id=' + encodeURIComponent(row.client_id) +
							'&client_secret=' + encodeURIComponent(row.client_secret) + '&grant_type=authorization_code' +
							'&redirect_uri=' + encodeURIComponent(row.redirect_uri) +
							'&code=' + encodeURIComponent(code);

						request(url, function (err, res, body) {
							if (!err && res.statusCode == 200) {
								var obj = JSON.parse(body);
								r.db(config.db.name).table(config.db.table).filter({
									name: req.user.name
								}).update({
									access_token: obj.access_token
								}).run(conn, function (err, res) {
									if (err) throw err;
									response.writeHead(302, {
										'Location': '/profile'
									});
									response.end();
								});
							}
						});
					});
				}
			});
		});
	});

	// Anything else goes to the frontend
	app.get('*', function (req, res) {
		res.sendfile('./public/index.html'); // load our public/views/index.html file
	});

};

function auth(req, res, next) {
	if (!req.isAuthenticated())
		res.send(401);
	else
		next();
}