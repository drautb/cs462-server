angular.module('app', ['ngRoute']).config(['$routeProvider', '$locationProvider', '$httpProvider',
	function ($routeProvider, $locationProvider, $httpProvider, $rootScope) {

		$httpProvider.responseInterceptors.push(function ($q, $location) {
			return function (promise) {
				return promise.then(
					// Success: just return the response 
					function (response) {
						return response;
					},
					// Error: check the error status to get only the 401 
					function (response) {
						if (response.status === 401)
							$location.url(response.config.url);
						return $q.reject(response);
					}
				);
			}
		});

		$routeProvider.when('/', {
			templateUrl: 'views/home.html'
		}).when('/user-list', {
			templateUrl: 'views/users.html',
			controller: 'UserController'
		}).when('/login', {
			templateUrl: 'views/login.html',
			controller: 'LoginController',
			resolve: {
				loggedin: checkLoggedin
			}
		}).when('/signup', {
			templateUrl: 'views/signup.html',
			controller: 'LoginController',
		}).when('/profile', {
			templateUrl: 'views/profile.html',
			controller: 'ProfileController',
			resolve: {
				loggedin: checkLoggedin
			}
		}).when('/user/:name', {
			templateUrl: 'views/user.html',
			controller: 'UserController'
		});

		$locationProvider.html5Mode(true);
	}
]);
// .run(function ($rootScope, $http) {
//  $rootScope.message = '';

//  // Logout function is available in any pages
//  $rootScope.logout = function () {
//      $rootScope.message = 'Logged out.';
//      $http.post('/logout');
//  };
// });

var checkLoggedin = function ($q, $timeout, $http, $location, $rootScope) {
	var deferred = $q.defer();
	$http.get('/loggedin').success(function (user) {
		if (user !== '0') {
			$timeout(deferred.resolve, 0);
			$location.url('/profile');
		} else {
			$rootScope.message = 'You need to log in.';
			$timeout(function () {
				deferred.reject();
			}, 0);
			$location.url('/login');
		}
	});
};