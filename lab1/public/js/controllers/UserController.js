angular.module('app').controller('UserController', function ($scope, $http, $routeParams) {

	$scope.user = {}

	$scope.loadData = function () {
		$http.get('/user-data/' + $routeParams.name).success(function (response) {
			$scope.user = response;
		});
	}

	$scope.findAll = function () {
		$http.get('/users').success(function (data) {
			$scope.users = data;
		}).error(function () {

		});
	};

});