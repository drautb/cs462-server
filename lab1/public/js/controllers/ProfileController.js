angular.module('app').controller('ProfileController', function ($scope, $http) {

	$scope.user = false;

	$scope.loadData = function () {
		$http.get('loggedin').success(function (res) {
			$scope.user = res !== 0 ? res : 0;
			$scope.user.access_token_trimmed = res.access_token.substring(0, 10) + "...";

			if ($scope.user.access_token !== "") {
				$http.get('https://api.foursquare.com/v2/users/self/checkins?oauth_token=' + $scope.user.access_token + "&v=20140131").success(function (res) {
					$scope.user.checkins = res.response.checkins.items;
				});
			}

		}).error(function (res) {
			$scope.user = 0;
		});
	};

});