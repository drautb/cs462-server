angular.module('app').controller('LoginController', function ($scope, $http, $location, $window) {

	$scope.message = ""

	$scope.signup = function () {
		var payload = {
			username: $scope.username,
			password: $scope.password
		};

		$http.post('/signup', payload).success(function (res) {
			$location.url('/profile');
		}).error(function (res) {
			$scope.message = "That username isn't available.";
			$scope.username = "";
			$scope.password = "";
		});
	}

	$scope.login = function () {
		var payload = {
			username: $scope.username,
			password: $scope.password
		};

		$http.post('/login', payload).success(function (res) {
			$window.location.href = '/profile';
		}).error(function (res) {
			$scope.message = "Invalid username/password";
			$scope.username = "";
			$scope.password = "";
		});
	};

	$scope.logout = function () {
		$http.post('/logout').success(function (res) {
			$location.url('/');
			$scope.loggedIn();
		}).error(function (res) {
			$location.url('/');
		});
	};

	$scope.loggedIn = function () {
		$http.get('/loggedin').success(function (user) {
			if (user !== '0') {
				$scope.isLoggedIn = user;
			} else {
				$scope.isLoggedIn = false;
			}
		});
	}

});