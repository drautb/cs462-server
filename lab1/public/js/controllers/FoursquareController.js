angular.module('app').controller('FoursquareController', function ($scope, $http, $window) {

	$scope.authorizeUrl = function () {
		$http.get('/foursquare-auth-url').success(function (res) {
			$scope.authUrl = res.url;
		}).error(function (res) {

		});
	}

});