'use strict'

var config = require('./config/config.json'),
	passport = require('passport'),
	express = require('express'),
	fs = require('fs'),
	https = require('https'),
	db = require('./app/db');

var app = express();

app.configure(function () {
	app.use(express.static(__dirname + '/public'));
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.cookieParser());
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.session({
		cookie: {
			maxAge: 3600000
		},
		secret: 'lwkefj9q38hfjsalkjf32qlkufhwi31'
	})); // session secret
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(app.router);
});

require('./app/passport')(passport); // pass passport for configuration
require('./app/routes')(app, passport); // configure our routes

db.setup();

var options = {
	key: fs.readFileSync(process.env.HOME + '/.ssl/server.key'),
	cert: fs.readFileSync(process.env.HOME + '/.ssl/server.crt'),
};

var server = https.createServer(options, app).listen(config.port, function () {
	console.log("Express server listening on port " + config.port);
});

exports = module.exports = app;