// RID: b505192x8
ruleset location_nearby {
	meta {
		name "Lab 7 - Semantic Translation"
		description <<
			My Lab 7 ruleset for CS 462
		>>
		author "Ben Draut"
		logging off
		key twilio {
		    "account_sid" : "AC4b08c7e9d22ae563161753ad9bfdf9fb",
		    "auth_token"  : "a149c50538019ed597c20d69478ef11a"
		}
		use module a8x115 alias twilio with twiliokeys = keys:twilio()
	}
	rule listen_nearby {
		select when explicit location_nearby
		pre {
			distance = event:attr("distance");
		}
		{	
			send_directive("Texting") with d = distance;
			twilio:send_sms("+18013181482", "+13852194383", "Distance: #{distance}");
		}
	}
}