// RID: b505192x7
//
//  https://cs.kobj.net/sky/event/2878C5B6-ADDC-11E3-B642-6545833561DC/8080/location/cur?_rids=b505192x7
//  https://cs.kobj.net/sky/event/2878C5B6-ADDC-11E3-B642-6545833561DC/8080/location/cur?_rids=b505192x7&lat=40.2693489441426&long=-111.650834083557
//  https://cs.kobj.net/sky/event/2878C5B6-ADDC-11E3-B642-6545833561DC/8080/location/cur?_rids=b505192x7

ruleset current_location {
	meta {
		name "Lab 7 - Semantic Translation"
		description <<
			My Lab 7 ruleset for CS 462
		>>
		author "Ben Draut"
		logging off
		use module b505192x5 alias location_data
	}
	rule nearby {
		select when location cur
		pre {
			//currentLat = location:latitude();
			//currentLong = location:longitude();
			
			currentLat = event:attr("lat");
			currentLong = event:attr("long");

			checkin_data = location_data:get_location_data("fs_checkin");
			checkinLat = checkin_data.pick("$..latitude");
			checkinLong = checkin_data.pick("$..longitude");

			r90 = math:pi() / 2;      
			rEm = 3963.1676;         // radius of the Earth in miles
			 
			// convert co-ordinates to radians
			rlata = math:deg2rad(currentLat);
			rlnga = math:deg2rad(currentLong);
			rlatb = math:deg2rad(checkinLat);
			rlngb = math:deg2rad(checkinLong);
			 
			// distance between two co-ordinates in miles
			dE = math:great_circle_distance(rlnga,r90 - rlata, rlngb,r90 - rlatb, rEm);
		}
		send_directive("current_location") with currentLat = currentLat and currentLong = currentLong and checkinLat = checkinLat and checkinLong = checkinLong and distance = dE;
		always {
			raise explicit event 'location_nearby' with distance = dE if dE < 5.0;
			raise explicit event 'location_far' with distance = dE if dE >= 5.0;
		}
	}
}